#include <stdio.h>
#include <string.h>
#define SIZE 100

int reverse(char []);

int main()
{
    char sen[SIZE];
    printf("Enter the sentence here, \n\t");
    fgets(sen, SIZE, stdin);

    reverse(sen);

    return 0;
}

int reverse(char sen[])
{
    int length=strlen(sen);
    printf("The reverse sentence -");
    for(int i=length-1; i>=0; i--)
    {
        printf("%c", sen[i]);
    }
    printf("\n\n");
    return 0;
}
