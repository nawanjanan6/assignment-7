#include <stdio.h>
#define SIZE 100

int main()
{
	int a[SIZE][SIZE];
	int b[SIZE][SIZE];
	int m,n,p,q;

	printf("Enter number of rows and columns in A: ");
	scanf("%d%d",&m,&n);
	printf("Enter number of rows and columns in B: ");
	scanf("%d%d",&p,&q);

	if(check(m, n , p, q)==1)
	{
		printf("Enter matrix A: ");
		for(int i=0;i<m;i++)
		{
		    for(int j=0;j<n;j++)
				scanf("%d", &a[i][j]);
		}

		printf("Enter matrix B: ");
		for(int i=0;i<p;i++)
        {
            for(int j=0;j<q;j++)
				scanf("%d", &b[i][j]);
        }

        add(a, b, m, n);
        multiply(a, b, m, n, p, q);

	}

	return 0;
}

int check(int m, int n, int p, int q)
{
    if(m!=p || n!=q)
	{
		printf("Matrix Addition is not possible");
		return 0;
	}
	else if(n!=p)
	{
		printf("Matrix Multiplication is not possible");
		return 0;
	}

	return 1;
}

int add(int a[SIZE][SIZE], int b[SIZE][SIZE],int m, int n)
{
    int c[SIZE][SIZE];
    for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
        }
		printf("\nAddition:\n");
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
}

int multiply(int a[SIZE][SIZE], int b[SIZE][SIZE],int m, int n, int p, int q)
{
    int d[SIZE][SIZE];
    for(int i=0;i<m;i++)
        {
            for(int j=0;j<q;j++)
            {
                for(int k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
            }
        }

		printf("\nMultiplication:\n");
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
}
