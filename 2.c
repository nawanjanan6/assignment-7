#include <stdio.h>
#include <string.h>
#define SIZE 100

void frequency(char []);

int main()
{
    char sen[SIZE];
    printf("Enter the sentence - ");
    fgets(sen, SIZE, stdin);

    frequency(sen);

    return 0;
}

void frequency(char sen[])
{
    printf("\nFrequency count character in this string , \n");

    int length = strlen(sen);
    int count=0;

    for(int i=0; i<length-1; i++)
    {
        count=1;
        if(sen[i] != '\0')
        {
            for(int j=i+1; j<length; j++)
            {
                if(sen[i]==sen[j])
                {
                    count++;
                    sen[j]='\0';
                }
            }
            printf(" '%c' ---- %d \n", sen[i], count);
        }
    }

    return 0;
}
